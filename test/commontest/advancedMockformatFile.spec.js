var chai = require('chai');
var expect = chai.expect;
var request = require('request');

describe('Advanced mockformat', function() {

    describe('File parameter', function() {

        it('Should return the response for the first filename match', function(done) {
            request.post('http://localhost:8080/api/advanced/file', function(err, res, body) {
                expect(res.statusCode).to.equal(200);
                expect(res.headers['content-type']).to.equal('application/json;charset=UTF-8');
                expect(JSON.parse(body)).to.deep.equal({name: 'OK a.pdf'});
                done();
            })
            .form().append('file', '<FILE_DATA>', {
                filename: 'a.pdf',
                contentType: 'application/pdf'
            });
        });

        it('Should return the response for the first mimeType match', function(done) {
            request.post('http://localhost:8080/api/advanced/file', function(err, res, body) {
                expect(res.statusCode).to.equal(201);
                expect(res.headers['content-type']).to.equal('application/json;charset=UTF-8');
                expect(JSON.parse(body)).to.deep.equal({mimeType: 'OK image/png'});
                done();
            })
            .form().append('file', '<FILE_DATA>', {
                filename: 'b.png',
                contentType: 'image/png'
            });
        });

        it('Should return the default response if no match found', function(done) {
            request.post('http://localhost:8080/api/advanced/file', function(err, res, body) {
                expect(res.statusCode).to.equal(203);
                expect(res.headers['content-type']).to.equal('application/json;charset=UTF-8');
                expect(JSON.parse(body)).to.deep.equal({name: 'safe-default.exe'});
                done();
            })
            .form().append('file', '<FILE_DATA>', {
                filename: 'ioweuroweiur.pdf',
                contentType: 'application/pdf'
            });
        });
    });

    describe('File parameters', function() {

        it('Should return the response for the first filename and mimeType match', function(done) {
            request.post('http://localhost:8080/api/advanced/file', function(err, res, body) {
                expect(res.statusCode).to.equal(202);
                expect(res.headers['content-type']).to.equal('application/json;charset=UTF-8');
                expect(JSON.parse(body)).to.deep.equal({name: 'OK c.pdf', mimeType: 'OK application/pdf'});
                done();
            })
            .form().append('file', '<FILE_DATA>', {
                filename: 'c.pdf',
                contentType: 'application/pdf'
            });
        });
    });

});